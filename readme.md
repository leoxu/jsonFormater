###欢迎使用 [jsonFormater](http://git.oschina.net/leoxu/jsonFormater)!

####源码来源：
跟osc的[json工具](http://tool.oschina.net/codeformat/json)使用的是同一套源代码，而osc的代码是从其会员 [SandKing](http://my.oschina.net/dengying) [那里](http://www.oschina.net/code/snippet_119610_11825)得到的。

####改进清单：
* 去掉了一些视觉上稍显多余，且与功能无关的东西；
* 具备一定程度上的对浏览器窗口大小变化的适应能力，没有浏览器滚动条，使用体验更好。